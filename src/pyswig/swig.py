##
## __legal_b__
##
## Copyright (c) 2020 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import subprocess

class Swig:
    """Swig helper class"""

    SWIG_SEARCV_ENV = ["SWIG", "ESYS_SWIG"]
    SWIG_EXE_NAME = ["swig.exe", "swig", "swigwin.exe"]

    LANGUAGES = ["python", "csharp", "xml"]

    def __init__(self):
        """Constructor"""
        self.m_path = None
        self.m_exe_path = None
        self.m_version = None
        self.m_verbose = False
        self.m_language = None
        self.m_process_cpp = False
        self.m_all_warnings = False
        self.m_input_file = None
        self.m_include_libs = None
        self.m_threads_enabled = False

    def set_include_libs(self, include_libs) -> None:
        """Set the pathes where to search for Swig include libraries

        Swig supports library files, which are included using the %include directive. 
        When searching for files, directories are searched in given order and among
        the pathes provided here.

        Args:
            include_libs: list of pathes to search for included libraries
        """
        self.m_include_libs = include_libs

    def add_include_lib(self, include_lib) -> None:
        """Add one path where to search for Swig include libraries

        

        Args:
            include_lib: a path to search for included libraries
        """
        if self.m_include_libs is None:
            self.m_include_libs = []
        self.m_include_libs.append(include_lib)

    def get_include_libs(self):
        """Get the pathes where to search for Swig include libraries 
        
        Returns:
            the pathes where to search for Swig include libraries
        """
        return self.m_include_libs

    def set_input_file(self, input_file: str) -> None:
        self.m_input_file = input_file

    def get_input_file(self) -> str:
        return self.m_input_file

    def set_all_warnings(self, all_warnings=True) -> None:
        """Turn on or off all warnings"""
        self.m_all_warnings = all_warnings

    def get_all_warnings(self) -> str:
        return self.m_all_warnings

    def set_threads_enabled(self, are_threads_enabled=True) -> None:
        """Turn on or off thread support"""
        self.m_threads_enabled = are_threads_enabled

    def get_threads_enabled(self) -> bool:
        return self.m_threads_enabled

    def set_process_cpp(self, process_cpp=True) -> None:
        """Turn on of off the processing of C++"""
        self.m_process_cpp = process_cpp

    def get_process_cpp(self) -> bool:
        return self.m_process_cpp

    def set_language(self, language: str) -> None:
        self.m_language = language

    def get_language(self) -> str:
        return self.m_language

    def log(self, msg: str) -> None:
        if self.get_verbose():
            print(msg)

    def set_verbose(self, verbose: bool) -> None:
        self.m_verbose = verbose

    def get_verbose(self) -> bool:
        return self.m_verbose

    def set_version(self, version: str) -> None:
        """Set Swig version"""
        self.m_version = version

    def get_version(self) -> str:
        """Get Swig version"""
        return self.m_version

    def get_exe_path(self) -> str:
        """Get the path to Swig executable"""
        return self.m_exe_path

    def set_exe_path(self, exe_path: str) -> None:
        """Set the path to Swig executable"""
        self.m_exe_path = exe_path

    def found_dir(self, path: str) -> bool:
        """Called when swig was found on some path"""
        self.m_path = os.path.abspath(path)
        for exe_name in Swig.SWIG_EXE_NAME:
            exe_path = self.m_path + os.sep + exe_name
            if os.path.exists(exe_path):
                self.set_exe_path(exe_path)
                return True
        return False

    def detect(self) -> bool:
        """Detect Swig
        
        Returns:
            True if found, False otherwise
        """
        if not self.detect_in_env():
            if not self.detect_in_exe_path():
                return False
        if self.detect_version() == 0:
            return True
        return False

    def detect_in_env(self) -> bool:
        """Try to find the swig executable using environment variable
        
        Returns:
            True if found, False otherwise
        """
        for swig_env in Swig.SWIG_SEARCV_ENV:
            if swig_env in os.environ:
                result = self.found_dir(os.environ.get(swig_env))
                if result:
                    return True
        return False

    def detect_in_exe_path(self) -> bool:
        """Try to find the swig executable hoping it's found in the path
        
        Returns:
            True if found, False otherwise
        """
        if "PATH" in os.environ:
            path_env = os.environ["PATH"]
            if path_env.find(":") != -1:
                paths = path_env.split(":")
            else:
                paths = path_env.split(":")
            for path in paths:
                result = self.found_dir(path)
                if result:
                    return True
        else:
            return False

    def detect_version(self) -> int:
        """Detect swig version
        
        Returns:
            0 if succesful, < 0 otherwise
        """
        if self.get_exe_path() is None:
            return -1
        result = subprocess.run([self.get_exe_path(), "-version"], stdout=subprocess.PIPE)
        if result.returncode != 0:
            return -2
        output = result.stdout.decode('UTF')
        found = -1
        line = None
        for line in output.split("\n"):
            found = line.find("SWIG Version")
            if found != -1:
                break
        if found == -1:
            return -3
        if line is None:
            return -4
        line = line.replace("SWIG Version", "")
        line = line.strip()
        self.set_version(line)
        return 0

    def run(self) -> int:
        """Run swig with the configured parameters
        
        Returns:
            0 if succesful, < 0 otherwise
        """
        if self.get_exe_path() is None:
            return -1
        cmd = [self.get_exe_path()]
        if self.get_all_warnings():
            cmd.append("-Wall")
        if self.get_process_cpp():
            cmd.append("-c++")
        if self.get_language() is not None:
            cmd.append("-" + self.get_language())
        if self.get_threads_enabled():
            cmd.append("-threads")
        if self.get_include_libs() is not None:
            for include_lib in self.get_include_libs():
                if os.path.isabs(include_lib):
                    cmd.append("-I" + include_lib)
                else:
                    cmd.append("-I" + os.getcwd() + os.sep + include_lib)
        if self.get_input_file() is not None:
            cmd.append(self.get_input_file())
        cmd_str = ""
        for i in cmd:
            cmd_str += i + " "
        print(cmd_str)
        result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = result.stdout.decode('UTF')
        if output != "":
            print(output) 
        err = result.stderr.decode('UTF')
        if err != "":
            print(err)
        if result.returncode != 0:
            return -2
        return 0
        
