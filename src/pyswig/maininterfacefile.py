##
## __legal_b__
##
## Copyright (c) 2020 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os

class MainInterfaceFile:
    """Class generating the main interface file, input to Swig"""
    def __init__(self, pyswig=None):
        self.m_pyswig = pyswig
        self.m_fout = None

    def __set_pyswig(self, pyswig) -> None:
        """Set the PySwig instance to use to generate the main interface file"""
        self.m_pyswig = pyswig

    def __get_pyswig(self):
        """Get the PySwig instance to use to generate the main interface file"""
        return self.m_pyswig

    def generate(self) -> None:
        """Generate the Swig interface file, the main input file for Swig"""
        self.open()
        self.write_header()
        self.write_cpp_includes()
        self.write_defines()
        self.main_file_write_interface_files()
        self.close()

    def open(self) -> int:
        """Open the Swig interface file, the main input file for Swig"""
        pyswig = self.__get_pyswig()
        if pyswig is None:
            return -1

        filename = pyswig.get_src_output_dir() + os.sep
        output_file = pyswig.get_module_name() + "." + "hh"
        filename += output_file
        pyswig.set_output_file(output_file)

        try:
            self.m_fout = open(filename, "w")
        except OSError as err:
            print("ERROR: %s" % err)
            return -2
        return 0

    def write_header(self):
        """Write header of the the main interface file"""
        pyswig = self.__get_pyswig()

        self.m_fout.write("// PySwig %s\n\n" % pyswig.get_version())
        if pyswig.get_use_director():
            self.m_fout.write("%%module(directors=\"1\") %s\n" % pyswig.get_module_name())
        else:
            self.m_fout.write("%%module %s\n" % pyswig.get_module_name())

        self.m_fout.write("%include typemaps.i\n")

        for type_map in pyswig.get_typemaps():
            self.m_fout.write("%%include %s\n" % type_map)

        self.m_fout.write("\n")
        if pyswig.get_imports() is not None:
            for import_lib in pyswig.get_imports():
                self.m_fout.write("%%import %s\n" % import_lib)
            self.m_fout.write("\n")

    def write_cpp_includes(self):
        """Write the C/C++ includes to the main file"""
        pyswig = self.__get_pyswig()

        self.m_fout.write("%{\n")
        self.m_fout.write("\n")
        for include in pyswig.get_includes(False):
            self.m_fout.write("#include <%s>\n" % include)
        self.m_fout.write("\n")
        for include in pyswig.get_includes(True):
            self.m_fout.write("#include \"%s\"\n" % include)
        self.m_fout.write("\n")
        self.m_fout.write("%}\n\n")

    def write_defines(self):
        """Write the defines in the main file"""
        pyswig = self.__get_pyswig()

        for define in pyswig.get_defines():
            if define[1]:
                self.m_fout.write("#define %s %s\n" % (define[0], define[1]))
            else:
                self.m_fout.write("#define %s\n" % define[0])
        self.m_fout.write("\n")

    def main_file_write_interface_files(self):
        """Write the insterface in the main file"""
        pyswig = self.__get_pyswig()

        for file_config in pyswig.get_file_configs():
            source_files = file_config.get_source_files()
            for source_file in source_files:
                wrap_filename = False
                filename = source_file
                if isinstance(source_file, list):
                    wrap_filename = filename[1]
                    filename = filename[0]
                filename_core, filename_ext = os.path.splitext(filename)
                if wrap_filename:
                    filename_core = filename_core + "_wrap"
                out_filename = filename_core + filename_ext
                self.m_fout.write("%%include %sh\n" % out_filename)

    def close(self):
        """Close the file"""
        if self.m_fout is not None:
            self.m_fout.close()
