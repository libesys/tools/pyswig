##
# __legal_b__
##
# Copyright (c) 2015-2020 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
##
# __legal_e__
##

import os
import sys
import argparse
import shutil
from pathlib import Path
from typing import Type, List

from pyswig.fileconfig import FileConfig
from pyswig.maininterfacefile import MainInterfaceFile
from pyswig.swig import Swig
from pyswig.processsrcfile import ProcessSrcFile


class PySwig:
    """The class used to configure the Swig preprocessor"""

    VERSION = "0.1.0"

    def __init__(self, verbose=True):
        """Constructor"""
        self.m_file_configs = []
        self.m_swig_include_dir = []
        self.m_args = None
        self.m_module_name = None
        self.m_use_director = False
        self.m_src_output_dir = None
        self.m_inc_output_dir = None
        self.m_typemaps = []
        self.m_global_include_files = []
        self.m_local_include_files = []
        self.m_defines = []
        self.m_output_file_ext = None
        self.m_version = PySwig.VERSION
        self.m_swig = Swig()
        self.m_output_file = None
        self.m_copy_shadow_dir = None
        self.m_replace_strings = []
        self.m_include_libs = None
        self.m_imports = None

        self.set_verbose(verbose)
        self.process_param()

    def set_imports(self, imports):
        self.m_imports = imports

    def add_import(self, import_lib) -> None:
        if self.m_imports is None:
            self.m_imports = []
        self.m_imports.append(import_lib)

    def get_imports(self):
        return self.m_imports

    def set_include_libs(self, include_libs):
        self.m_include_libs = include_libs

    def add_include_lib(self, include_lib) -> None:
        if self.m_include_libs is None:
            self.m_include_libs = []
        self.m_include_libs.append(include_lib)

    def get_include_libs(self):
        return self.m_include_libs

    def add_replace_string(self, orig, new):
        """Add string to be replace in the wrapper source code gerenated by Swig"""
        self.m_replace_strings.append([orig, new])

    def get_replace_strings(self):
        return self.m_replace_strings

    def get_shadow_file(self) -> str:
        """Get the shadow file name if any is build"""
        if self.get_swig().get_language() == "python":
            return self.get_module_name() + ".py"
        return None

    def set_copy_shadow_dir(self, copy_shadow_dir: str) -> None:
        """Set the folder where the shadow class is copied to if any"""
        self.m_copy_shadow_dir = copy_shadow_dir

    def get_copy_shadow_dir(self) -> str:
        """Get the folder where the shadow class is copied to if any"""
        return self.m_copy_shadow_dir

    def set_output_file(self, output_file: str) -> None:
        """Set the outfile for Swig"""
        self.m_output_file = output_file

    def get_output_file(self) -> str:
        """Get the outfile for Swig"""
        return self.m_output_file

    def set_all_warnings(self, all_warnings=True) -> None:
        """Turn on or off all warnings"""
        self.m_swig.set_all_warnings(all_warnings)

    def get_all_warnings(self) -> bool:
        """Get if all warnings are turned on or off """
        return self.m_swig.get_all_warnings()

    def set_threads_enabled(self, are_threads_enabled=True) -> None:
        """Turn on or off thread support"""
        self.m_swig.set_threads_enabled(are_threads_enabled)

    def get_threads_enabled(self) -> bool:
        """Get if swig will generate theaded wrapper ot not"""
        return self.m_swig.get_threads_enabled()

    def set_process_cpp(self, process_cpp=True) -> None:
        """Turn on of off the processing of C++"""
        self.m_swig.set_process_cpp(process_cpp)

    def get_process_cpp(self) -> bool:
        """Get if Swig will be processing of C++ is turned on or off"""
        return self.m_swig.get_process_cpp()

    def set_language(self, language: str) -> None:
        """Set the language that the wrapper Swig should create"""
        self.m_swig.set_language(language)

    def get_language(self) -> str:
        """Get the language that the wrapper Swig should create"""
        return self.m_swig.get_language()

    def get_swig(self) -> Type[Swig]:
        """Get the Swig instance"""
        return self.m_swig

    def set_verbose(self, verbose: bool) -> None:
        """Set the verbosity level"""
        self.m_verbose = verbose
        self.m_swig.set_verbose(verbose)

    def get_verbose(self) -> bool:
        """Get the verbosity level"""
        return self.m_verbose

    def get_version(self) -> str:
        """Get PySwig version"""
        return self.m_version

    def set_output_file_ext(self, ext: str) -> None:
        """Set the extension that the generated files input to Swig should have"""
        self.m_output_file_ext = ext

    def get_output_file_ext(self) -> str:
        """get the extension that the generated files input to Swig should have"""
        return self.m_output_file_ext

    def set_module_name(self, module_name: str) -> None:
        """Set the Swig module name"""
        self.m_module_name = module_name

    def get_module_name(self) -> str:
        """Get the Swig module name"""
        return self.m_module_name

    def set_use_director(self, use_director: bool) -> None:
        """Set if the director feature of Swig should be used or not"""
        self.m_use_director = use_director

    def get_use_director(self) -> bool:
        """Get if the director feature of Swig should be used or not"""
        return self.m_use_director

    def set_src_output_dir(self, src_output_dir: str) -> None:
        """Set the output directory for source file"""
        self.m_src_output_dir = src_output_dir

    def get_src_output_dir(self) -> str:
        """Get the output directory for source file"""
        return self.m_src_output_dir

    def set_inc_output_dir(self, inc_output_dir: str) -> None:
        """Set the output directory for include files"""
        self.m_inc_output_dir = inc_output_dir

    def get_inc_output_dir(self) -> str:
        """Get the output directory for include files"""
        return self.m_inc_output_dir

    def add_typemap(self, type_map: str) -> None:
        """Add a typemap which should be included in the generated files"""
        self.m_typemaps.append(type_map)

    def get_typemaps(self) -> List[str]:
        """Get the list of all typemaps to use"""
        return self.m_typemaps

    def add_include(self, include: str, local=True) -> None:
        """Add a C/C++ include file to be added"""
        if local:
            self.m_local_include_files.append(include)
        else:
            self.m_global_include_files.append(include)

    def get_includes(self, local=True) -> List[str]:
        """Get the list of include files to be addded to the generated files"""
        if local:
            return self.m_local_include_files
        return self.m_global_include_files

    def add_define(self, name: str, value="") -> None:
        """Adds a define to be included in the generated files"""
        if value == "":
            value = None
        self.m_defines.append([name, value])

    def get_defines(self) -> List[str]:
        """Get the list of all defines to be included in the generated files"""
        return self.m_defines

    def add_file_config(self, file_config: str) -> None:
        """Add a FileConfig object"""
        self.m_file_configs.append(file_config)
        file_config.set_pyswig(self)

    def get_file_configs(self) -> List[str]:
        """Return the list of all FileConfig objects"""
        return self.m_file_configs

    def process_param(self) -> None:
        """Process the command line parameters"""
        parser = argparse.ArgumentParser()
        parser.add_argument("input_file", default=None,
                            nargs='*', help="input file")
        parser.add_argument("--copy-shadow", dest="m_copy_shadow_dir", action="store",
                            help="set the folder where the shadow file should be copied")
        args = parser.parse_args(sys.argv)
        self.m_args = args
        if args.m_copy_shadow_dir is not None:
            self.set_copy_shadow_dir(args.m_copy_shadow_dir)

    def add_include_dir(self, include_dir: str) -> None:
        """Add an include directory"""
        self.m_swig_include_dir.append(include_dir)

    def generate(self) -> None:
        """Generate all output files"""
        print("PySwig %s\n" % self.get_version())

        detected = self.m_swig.detect()
        if detected:
            print("Swig %s detected.\n" % self.m_swig.get_version())
        else:
            print("Couldn't find Swig.\n")

        self.handle_input_file()

        for file_config in self.get_file_configs():
            self.do_file_config(file_config)

        self.generate_main_interface_file()
        self.generate_include_file()

    def do_file_config(self, file_config: Type[FileConfig]) -> None:
        """Generate the output files related to one given FileConfig"""
        if file_config.get_src_output_dir(False) is not None:
            temp_folder = file_config.get_src_output_dir(False)
        else:
            temp_folder = "temp_pyswig"
        try:
            os.mkdir(temp_folder)
        except:
            pass

        if file_config.get_source_files() is not None:
            for filename in file_config.get_source_files():
                wrap_filename = False
                if isinstance(filename, list):
                    wrap_filename = filename[1]
                    filename = filename[0]
                if os.path.isdir(file_config.m_input_dir + os.sep + filename):
                    pass
                else:
                    obj = ProcessSrcFile(filename, self.m_verbose, file_config, wrap_filename)
                    obj.set_pyswig_version(self.get_version())
                    obj.parse()
        else:
            if len(self.m_args) > 1:
                name = self.m_args[1]

                obj = ProcessSrcFile(name)
                obj.set_pyswig_version(self.get_version())
                obj.parse()
            else:
                print("ERROR: must provide a file")

    def handle_input_file(self) -> None:
        """Change the directory if required, depending of the input file, etc."""
        if len(self.m_args.input_file) != 1:
            print("ERROR: more than one input file was given")
            raise Exception("ERROR: more than one input file was given")

        print("Input file : %s" % self.m_args.input_file[0])

        the_path = Path(self.m_args.input_file[0])
        if len(the_path.parts) == 1:
            # This means that we are already in the folder where the inlcude file is located,
            # so nothing needed to be do, we are already in the right folder
            print("")
            return
        parent_path = the_path.parent
        print("chdir %s" % parent_path)
        os.chdir(str(parent_path))
        print("")

    def generate_include_file(self) -> None:
        """Write the include file included in the wrapper if generated"""
        if self.get_inc_output_dir() is None:
            return
        filename = os.path.normpath(self.get_inc_output_dir()) + os.sep
        filename += self.get_module_name() + "_inc.h"
        print("Creating %s" % filename)
        fout = open(filename, "w")
        fout.write("// PySwig %s\n\n" % self.get_version())
        fout.write("#pragma once\n")
        fout.write("\n")

        for file_config in self.get_file_configs():
            base_dir = file_config.get_base_inc_dir()
            for filename in file_config.get_source_files():
                wrap_filename = False
                if isinstance(filename, list):
                    wrap_filename = filename[1]
                    filename = filename[0]
                if base_dir is not None:
                    fout.write("#include \"%s/%s\"\n" % (base_dir, filename))
                else:
                    fout.write("#include \"%s\"\n" % filename)
        fout.write("\n\n")
        fout.close()

    def generate_main_interface_file(self) -> None:
        """Write the Swig interface file, the main input file for Swig"""
        main_if_file = MainInterfaceFile(self)
        main_if_file.generate()

    def run_swig(self) -> int:
        """Run swig on the newly generated files"""
        print("\nRunning Swig ...")
        print("chdir %s" % self.get_src_output_dir())
        os.chdir(self.get_src_output_dir())
        self.get_swig().set_input_file(self.get_output_file())

        self.get_swig().set_include_libs(self.get_include_libs())

        result = self.get_swig().run()
        if result == 0:
            print("Success.")
        else:
            print("Failed.")

        if self.get_copy_shadow_dir() is not None and self.get_shadow_file() is not None:
            print("copy file %s to %s" %
                  (self.get_shadow_file(), self.get_copy_shadow_dir()))
            shutil.copy(self.get_shadow_file(), self.get_copy_shadow_dir())

        return result

    def get_output_cxx_filename(self):
        return self.get_module_name() + "_wrap.cxx"

    def get_output_cxx_file_path(self):
        file_path = self.get_src_output_dir()
        file_path += os.sep
        file_path += self.get_output_cxx_filename()

        return file_path

    def replace_strings_in_line(self, line):
        for rep in self.get_replace_strings():
            line = line.replace(rep[0], rep[1])
        return line

    def replace_all_strings(self):
        """Replace all strings in Swig generated wrapper file"""
        print("\nReplacing strings in %s ..." % self.get_output_cxx_filename())
        print("chdir %s" % self.get_src_output_dir())

        filename = self.get_output_cxx_filename()
        shutil.move(filename, filename + ".orig")

        fin = open(filename + ".orig", "r")
        fin_lines = fin.readlines()
        fin.close()

        fout = open(filename, "w")
        for line in fin_lines:
            new_line = self.replace_strings_in_line(line)
            fout.write(new_line)

        fout.close()
