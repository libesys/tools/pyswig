##
## __legal_b__
##
## Copyright (c) 2015-2020 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import inspect

class ProcessSrcFile:
    """Process one FileConfig"""
    def __init__(self, name, verbose=False, file_config=None, wrap_filename=False):
        """Constructor"""
        self.m_name = name
        self.m_state = 0
        self.m_line_nbr = 0
        self.m_file_config = file_config
        self.m_file_in = None
        self.m_fin = None
        self.m_fout = None
        self.m_debug = False
        self.m_verbose = verbose
        self.m_found_tags = False
        self.m_tags = []
        self.m_line = None
        self.m_comment = -1
        self.m_cmd_i = -1
        self.m_el_str = None
        self.m_cmdname = None
        self.m_str = None
        self.m_pyswig_version = None
        self.m_write_file_line = False # If True, write the file and line number in the output file
        self.m_error_count = 0
        self.m_wrap_filename = wrap_filename

    def get_wrap_filename(self):
        return self.m_wrap_filename

    def get_error_count(self):
        return self.m_error_count

    def set_pyswig_version(self, pyswig_version) -> None:
        self.m_pyswig_version = pyswig_version

    def get_pyswig_version(self):
        return self.m_pyswig_version

    def add_include(self) -> None:
        """Add a C/C++ include file at the place of <swig_inc/> annotation
        """
        line = self.m_line.replace("//<swig_inc/>", "//<swig_inc>")
        self.m_line = self.m_line.replace("//<swig_inc/>", "//</swig_inc>")
        self.m_fout.write(line)
        self.m_fout.write("%{\n")
        self.m_fout.write("#include \""+self.m_file_config.m_base_inc_dir+"/"+self.m_name+"\"\n")
        self.m_fout.write("%}\n")

    def write(self, text: str) -> None:
        """Write some text to the ouput file
        
        Args:
            text: the text to write
        """
        if self.m_write_file_line:
            cur_frame = inspect.currentframe()
            filename = inspect.getframeinfo(cur_frame).filename
            lineno = cur_frame.f_back.f_lineno
            footer = "line %s, file %s" % (lineno, filename)
            text = text[:-1] + "    //" + footer + "\n"
        self.m_fout.write(text)

    def __parse_open_files(self) -> None:
        """Open both the input and output file"""
        file_in = self.m_name
        if self.m_file_config is not None: 
            if self.m_file_config.m_input_dir is not None:
                file_in = self.m_file_config.m_input_dir +os.sep + self.m_name
            if self.m_file_config.get_base_abs_path() is not None:
                file_in = self.m_file_config.get_base_abs_path() + os.sep + file_in
    
        file_in = os.path.normpath(file_in)
        try:
            self.m_fin = open(file_in, "r")
        except OSError: 
            self.m_fin = None
            self.m_file_in = None
            return -1

        self.m_file_in = file_in

        i = self.m_name.rfind(".")

        name = self.m_name[:i]
        ext = None
        if (self.m_file_config is not None) and (self.m_file_config.get_output_file_ext(False) is not None):
            ext = self.m_file_config.get_output_file_ext(False)
        if ext is None:
            ext = "i"
        if self.get_wrap_filename():
            name = name + "_wrap"
        name = name + "." + ext

        file_out_name = name
        if self.m_file_config is None:
            return 0
        if not self.m_file_config.m_do_swig:
            return 0

        if self.m_file_config.get_src_output_dir(False) is not None:
            file_out_name = self.m_file_config.get_src_output_dir(False) + os.sep + name
        if self.m_file_config.get_base_abs_path() is not None:
            file_out_name = self.m_file_config.get_base_abs_path() + os.sep + file_out_name
        file_out_name = os.path.normpath(file_out_name)
        dir_out = os.path.split(file_out_name)[0]
        if self.m_verbose:
            file_path = dir_out + os.sep + name
            file_path = file_path.replace("/", os.sep)
            file_path = file_path.replace("\\", os.sep)
            print("Creating %s" % file_path)
        if (dir_out != "") and (not os.path.isdir(dir_out)):
            os.makedirs(dir_out)
        try:
            self.m_fout = open(file_out_name, "w")
        except OSError:
            self.m_fout = None
            return -2

        if self.get_pyswig_version() is not None:
            self.m_fout.write("// PySwig %s\n\n" % self.get_pyswig_version())
        else:
            self.m_fout.write("// PySwig unknwon version\n\n")
        return 0

    def start_of_single_line_comment(self) -> None:
        """Handle the start of a single line comment"""
        self.m_comment = 0
        if self.m_debug:
            print("found a line comment")
        i = self.m_line.find("//<")
        #self.m_fout.write(self.m_line[:i])
        el_str = self.m_line[i+3:]

        j = el_str.find(">")
        if j == -1:
            #self.m_fout.write(self.m_line[i:])
            pass
        else:
            self.m_cmdname = el_str[:j]
            self.m_cmd_i = i + 3
            self.m_el_str = el_str
            if self.m_debug:
                print("cmdname = %s" % self.m_cmdname)
            #self.do_command(cmdname)

    def start_of_multi_lines_comment(self) -> None:
        """Handle the start of a multi lines comment"""
        self.m_comment = 1
        if self.m_debug:
            print("found a block comment")
        i = self.m_line.find("/*<")
        #self.m_fout.write(self.m_line[:i])
        el_str = self.m_line[i+3:]

        j = el_str.find(">")
        if j == -1:
            #self.m_fout.write(self.m_line[i:])
            pass
        else:
            self.m_cmdname = el_str[:j]
            self.m_cmd_i = i + 3
            self.m_el_str = el_str
            if self.m_debug:
                print("cmdname=%s" %self.m_cmdname)
            #self.do_command(cmdname)

    def handle_comment(self) -> None:
        """Handle one comment"""
        if self.m_state == 0:
            #if nothing is found the whole line is simply written to the ouput file
            if self.m_cmd_i != -1:
                self.do_command(self.m_cmdname)
            else:
                if self.m_file_config.m_do_swig:
                    self.write(self.m_line)
        elif self.m_state == 1:
            #we are in a commentout block, so comment out all the block
            #print "state 1"
            result = True
            if self.m_cmd_i != -1:
                result = self.do_command(self.m_cmdname)
            if (self.m_cmd_i == -1) | (not result):
                self.m_line = self.m_line.replace("/*", " *")
                self.m_line = self.m_line.replace("*/", "* ")
                if self.m_file_config.m_do_swig:
                    self.write(self.m_line)
        elif self.m_state == 2:
            #print "state 2"
            result = True
            if self.m_cmd_i != -1:
                result = self.do_command(self.m_cmdname)
            if (self.m_cmd_i == -1) | (not result):
                if self.m_file_config.m_do_swig:
                    self.write(self.m_line)
        elif self.m_state == 3:
            #if nothing is found the whole line is simply written to the ouput file
            if self.m_cmd_i != -1:
                self.do_command(self.m_cmdname)
            else:
                if self.m_file_config.m_do_swig:
                    self.write(self.m_line)
                self.m_tags = self.m_tags + self.m_line.strip(",")

    def parse(self) -> None:
        """"Parse this input file and generated the corresponding output file"""
        result = self.__parse_open_files()
        if result < 0:
            return result

        if self.m_fin is None:
            print("ERROR: failed to open the input file")
            return -2
        if self.m_fout is None:
            print("ERROR: failed to open the output file")
            return -3

        self.m_line = self.m_fin.readline()
        self.m_line_nbr = self.m_line_nbr + 1
        self.m_state = 0
        while self.m_line:
            self.m_comment = -1
            self.m_cmd_i = -1
            if self.m_line.find("//<swig_inc/>") != -1:
                self.add_include()
            elif self.m_line.find("//<") != -1:
                self.start_of_single_line_comment()
            elif self.m_line.find("/*<") != -1:
                self.start_of_multi_lines_comment()
            else:
                #self.m_fout.write(self.m_line)
                pass
            self.handle_comment()

            self.m_line = self.m_fin.readline()
            self.m_line_nbr = self.m_line_nbr+1
        self.m_fin.close()
        if self.m_file_config.m_do_swig:
            self.m_fout.close()
        if not self.m_found_tags:
            self.m_file_config.add_tags(self.m_file_in, None)
        if self.get_error_count() == 0: 
            return 0
        return -4

    def __command_commentout_helper000(self) -> None:
        """Helper function"""
        if self.m_comment == 0:
            if self.m_file_config.m_do_swig:
                self.m_fout.write("//<")
        else:
            if self.m_file_config.m_do_swig:
                self.m_fout.write("/*<")
        if self.m_file_config.m_do_swig:
            self.m_fout.write(self.m_el_str)

    def print_error(self, msg: str) -> None:
        """Print an error message adding in which file and line the error occured"""
        header = "ERROR file %s line %i: " % (self.m_file_in, self.m_line_nbr)
        print(header + msg)
        self.m_error_count += 1

    def do_swig_out_start(self, cmd: str) -> None:
        """Process a <swig_out>"""
        if self.m_state != 0:
            self.print_error("<%s> can't be nested inside another pyswig construct" % cmd)

        if True:
            #means that the commenout block is commented out
            if self.m_file_config.m_do_swig:
                self.m_fout.write("/* ")
                self.m_fout.write(self.m_line)
            self.m_state = 1
            #self.command_commentout_helper000()
        else:
            #means that the commenout block is written in the output file at all
            self.m_state = 2
            self.__command_commentout_helper000()

    def do_swig_out_close(self, cmd: str) -> bool:
        """Process a </swig_out>"""
        if self.m_state == 1:
            #self.command_commentout_helper000()
            if self.m_file_config.m_do_swig:
                self.m_fout.write(self.m_line.strip("\n")+" */\n")
        if (self.m_state == 1) | (self.m_state == 2):
            self.m_state = 0
            return True

        self.print_error("<%s> without previous opening statement <%s>" % (cmd, cmd[1:]))
        return False

    def do_swig_out_closed(self) -> None:
        """Process a <swig_out/>"""
        if self.m_file_config.m_do_swig:
            self.m_fout.write("// ")
        self.m_fout.write(self.m_line)

    def do_swig_start(self, cmd: str) -> bool:
        """Process a <swig>"""
        if self.m_state != 0:
            self.print_error("<%s> can't be nested inside another pyswig construct" % cmd)
        if self.m_line.find("</swig>") != -1:
            end = self.m_line.find("</swig>")
            begin = self.m_cmd_i + 5
            if self.m_file_config.m_do_swig:
                self.m_fout.write(self.m_line[begin:end] + "    " + self.m_line)
            self.m_state = 0
            return True

        if self.m_file_config.m_do_swig:
            self.m_fout.write(self.m_line.replace("/*", "//"))
        self.m_state = 2
        return True

    def do_swig_close(self, cmd: str) -> bool:
        """Process a </swig>"""
        if self.m_state != 2:
            self.print_error("<%s> was found without previous <%s>" % (cmd, cmd[1:]))
        if self.m_file_config.m_do_swig:
            self.write(self.m_line.replace("*/", ""))
        self.m_state = 0
        return True

    def do_swig_closed(self, cmd: str) -> bool:
        """Process a <swig/>"""
        if self.m_file_config.m_do_swig:
            line = self.m_line.replace("//<swig/>", "")
            self.m_fout.write(line)
        else:
            self.m_fout.write(self.m_line)
        return True

    def do_tags_start(self, cmd: str) -> None:
        """Process a <tags>"""
        self.m_found_tags = True
        if self.m_state != 0:
            self.print_error("<tags> can't be nested inside another pyswig construct")

        if self.m_line.find("</tags>") != -1:
            end = self.m_line.find("</tags>")
            begin = self.m_cmd_i + 5
            self.m_tags = self.m_tags + self.m_line[begin:end].replace(" ", "").split(",")
            self.m_file_config.AddTags(self.m_file_in, self.m_tags)
        else:
            begin = self.m_cmd_i+5
            self.m_tags = self.m_tags + self.m_line[begin:].split(",")
            self.m_state = 3

    def do_tags_close(self, cmd: str) -> None:
        """Process a </tags>"""
        self.m_state = 0
        end = self.m_line.find("</tags>")
        begin = self.m_cmd_i + 5
        self.m_tags = self.m_tags + self.m_line[begin:end].replace(" ", "").split(",")
        self.m_file_config.AddTags(self.m_file_in, self.m_tags)

    def do_command(self, cmd: str) -> bool:
        """Process one given command"""
        if cmd in ["commentout", "swig_out"]:
            return self.do_swig_out_start(cmd)

        if cmd in ["/commentout", "/swig_out"]:
            return self.do_swig_out_close(cmd)

        if cmd == "swig_out/":
            return self.do_swig_out_closed()

        if cmd == "swig":
            return self.do_swig_start(cmd)

        if cmd == "/swig":
            return self.do_swig_close(cmd)

        if cmd == "swig/":
            return self.do_swig_closed(cmd)

        if cmd == "swig_interface":
            if self.m_state != 0:
                self.print_error("swig_interface> can't be nested inside another pyswig construct")
            # s = self.m_line[self.m_cmd_i:]
            # s = s[s.find(" "):]
            # i = s.find("name")
            # self.m_out_interface=open(
        elif cmd == "/swig_interface":
            pass
        elif cmd == "tags":
            self.do_tags_start(cmd)

        elif cmd == "/tags":
            self.do_tags_close(cmd)

        else:
            self.print_error("unknown command <%s> was found" % cmd)
        return False
