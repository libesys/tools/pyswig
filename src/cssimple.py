#!/usr/bin/env python3

##
## __legal_b__
##
## Copyright (c) 2018-2020 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import sys

# Some tricks only needed to simplify the development of PySwig. This
# is not nedeed if PySwig was intalled with pip
if "PYSWIG" in os.environ:
    PYSWIG_DIR = os.path.abspath(os.environ.get("PYSWIG"))
    print("pySwig found at %s" % PYSWIG_DIR)
    sys.path.append(PYSWIG_DIR)

import pyswig

# Welcoming messages
print("cssimple.py begins ...\n")

# Create a configuration of files to process
file_config = pyswig.FileConfig()

# Tells the location relative to this Python file, where the header files are found
file_config.set_input_dir("../include/simple")

# Set the beginning of the path used by the <swig_inc/> annotation, which will 
# be for example #include <simple/version.h> for the first source file
# given below
file_config.set_base_inc_dir("simple")

# Path to all sources, but relative to the input directory given above in set_input_dir
source = ["version.h",
          "object.h"
          ]

# Add the source files
file_config.set_source_files(source)

# Create a PySwig instance
pyswig_obj = pyswig.PySwig()

# Add to it the first configuration of files to process
pyswig_obj.add_file_config(file_config)

# Create an extra configuration of files to process. This is 
# just an example on how to do this, this is not mandatory
file_config_extra = pyswig.FileConfig()

# Tells the location relative to this file, where those extra header files are found
file_config_extra.set_input_dir("../include/cssimplewrap")

# Paths to all extra sources, but relative to the input directory given above
source_extra = ["csversion.h"
                ]
            
# Add the extra source files
file_config_extra.set_source_files(source_extra)

# Add the configuration of extra files to process
pyswig_obj.add_file_config(file_config_extra)

# Configure the generation of the input files for Swig.
# This give the name of the Python module to create
pyswig_obj.set_module_name("cssimplewrap")

# This module will use the director feature of Swig
pyswig_obj.set_use_director(True)

# Set the folder where all output source files will be store, relative to this Python script
pyswig_obj.set_src_output_dir("./cssimplewrap")

# Set the folder where all output header files will be store, relative to this Python script
pyswig_obj.set_inc_output_dir("../include/cssimplewrap")

# Set the file extension to be used for all Swig input files. If not specified
# the usual "i" extension will be used
pyswig_obj.set_output_file_ext("hh")

# Typemaps files to be added to the Swig input files
pyswig_obj.add_typemap("std_string.i")
pyswig_obj.add_typemap("std_vector.i")
pyswig_obj.add_typemap("stdint.i")

# Include file to be added to the Swig input files, False means that it's a
# system include file and will added as #include <..>
pyswig_obj.add_include("stdio.h", False)

# Include file to be added to the Swig input files, with no parameter means that it's a
# local include file and will added as #include ".."
pyswig_obj.add_include("simple/simple_defs.h")

# Add empty defines so Swig can parse the generated input files
pyswig_obj.add_define("SIMPLE_API")
pyswig_obj.add_define("CSTESTWRAP_API")

# And finally generate the Swig input files
pyswig_obj.generate()

# Swig will show all warnings
pyswig_obj.set_all_warnings()

# Swig will process the files as C++ files
pyswig_obj.set_process_cpp()

# Swig will create a C# wrapper
pyswig_obj.set_language("csharp")

# And finally run Swig, which will then generate the source files of the wrapper
pyswig_obj.run_swig()

# Simply tells we are done
print("cssimple.py ends.")

