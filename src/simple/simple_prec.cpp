/*!
 * \file simple/simple_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

// test.cpp : source file that includes just the standard includes
// test.pch will be the pre-compiled header
// test.obj will contain the pre-compiled type information

#include "simple/simple_prec.h"

// TODO: reference any additional headers you need in test_prec.h
// and not in this file

