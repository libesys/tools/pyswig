/*!
 * \file simple/object.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "simple/simple_prec.h"
#include "simple/object.h"

namespace simple
{

Object::Object()
{
}

Object::Object(const std::string &name): m_name(name)
{
}

Object::~Object()
{
}

void Object::set_name(const std::string &name)
{
    m_name = name;
}

std::string &Object::get_name()
{
    return m_name;
}

const std::string &Object::get_name() const
{
    return m_name;
}

int Object::set_data(uint8_t *data, uint32_t size)
{
    if (size > 256)
        return -1;

    memcpy(m_data, data, size);
    m_size = size;
    return 0;
}

int Object::get_data(uint8_t *data, uint32_t &size)
{
    if (size < m_size)
        return -1;

    memcpy(data, m_data, m_size);
    return 0;
}

uint8_t *Object::get_data(uint32_t &size)
{
    size = m_size;
    return m_data;
}

}