// PySwig 0.0.1

/*!
 * \file cssimplewrap/csversion.h
 * \brief Version info for cssimplewrap
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define CSSIMPLEWRAP_MAJOR_VERSION    0
#define CSSIMPLEWRAP_MINOR_VERSION    0
#define CSSIMPLEWRAP_RELEASE_NUMBER   1
#define CSSIMPLEWRAP_VERSION_STRING   "cssimplewrap 0.0.1"

// Must be updated manually as well each time the version above changes
#define CSSIMPLEWRAP_VERSION_NUM_DOT_STRING   "0.0.1"
#define CSSIMPLEWRAP_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define CSSIMPLEWRAP_VERSION_NUMBER (CSSIMPLEWRAP_MAJOR_VERSION * 1000) + (CSSIMPLEWRAP_MINOR_VERSION * 100) + CSSIMPLEWRAP_RELEASE_NUMBER
#define CSSIMPLEWRAP_BETA_NUMBER      1
#define CSSIMPLEWRAP_VERSION_FLOAT CSSIMPLEWRAP_MAJOR_VERSION + (CSSIMPLEWRAP_MINOR_VERSION/10.0) + (CSSIMPLEWRAP_RELEASE_NUMBER/100.0) + (CSSIMPLEWRAP_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define CSSIMPLEWRAP_CHECK_VERSION(major,minor,release) \
    (CSSIMPLEWRAP_MAJOR_VERSION > (major) || \
    (CSSIMPLEWRAP_MAJOR_VERSION == (major) && CSSIMPLEWRAP_MINOR_VERSION > (minor)) || \
    (CSSIMPLEWRAP_MAJOR_VERSION == (major) && CSSIMPLEWRAP_MINOR_VERSION == (minor) && CSSIMPLEWRAP_RELEASE_NUMBER >= (release)))


