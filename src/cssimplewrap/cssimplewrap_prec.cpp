/*!
 * \file cssimplewrap/cssimplewrap_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

// cstestwrap.cpp : source file that includes just the standard includes
// cstestwrap.pch will be the pre-compiled header
// cstestwrap.obj will contain the pre-compiled type information

#include "cssimplewrap/cssimplewrap_prec.h"

// TODO: reference any additional headers you need in cstestwrap_prec.h
// and not in this file

