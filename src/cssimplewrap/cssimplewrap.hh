// PySwig 0.0.1

%module(directors="1") cssimplewrap
%include typemaps.i
%include std_string.i
%include std_vector.i
%include stdint.i
%{

#include <stdio.h>

#include "simple/simple_defs.h"

%}

#define SIMPLE_API
#define CSTESTWRAP_API

%include version.hh
%include object.hh
%include csversion.hh
