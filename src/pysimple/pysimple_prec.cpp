/*!
 * \file pysimple/pysimple_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// pytest.cpp : source file that includes just the standard includes
// pytest.pch will be the pre-compiled header
// pytest.obj will contain the pre-compiled type information

#include "pysimple/pysimple_prec.h"

// TODO: reference any additional headers you need in pytest_prec.h
// and not in this file

