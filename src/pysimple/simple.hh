// PySwig 0.1.0

%module(directors="1") simple
%include typemaps.i
%include std_string.i
%include std_vector.i
%include stdint.i

%{

#include <stdio.h>

#include "simple/simple_defs.h"

%}

#define SIMPLE_API
#define PYSIMPLE_API

%include version.hh
%include object.hh
%include pyversion.hh
