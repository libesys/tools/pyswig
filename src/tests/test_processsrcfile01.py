import unittest
import pyswig
import os

class TestFileConfig01(unittest.TestCase):
    def test_load_file(self):
        file_config = pyswig.FileConfig()

        script_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "pysimple.py")
        file_config.set_script_path(script_path)

        # Tells the location relative to this Python file, where the header files are found
        file_config.set_input_dir("../include/simple")

        # Set the folder where all output source files will be store, relative to this Python script
        file_config.set_src_output_dir("./pysimple")

        # Set the beginning of the path used by the <swig_inc/> annotation, which will 
        # be for example #include <simple/version.h> for the first source file
        # given below
        file_config.set_base_inc_dir("simple")

        # Path to all sources, but relative to the input directory given above in set_input_dir
        source = ["version.h",
            "object.h"        
            ]
        
        # Add the source files
        file_config.set_source_files(source)

        obj = pyswig.ProcessSrcFile(file_config.get_source_files()[0], True, file_config)
        obj.set_pyswig_version("13.0.1")
        result = obj.parse()
        self.assertEqual(result, 0)

    
if __name__ == "__main__":
    unittest.main()