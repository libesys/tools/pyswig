Welcome to PySwig's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   pyswig

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
