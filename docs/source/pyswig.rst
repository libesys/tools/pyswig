Documentation
=================

The ``PySwig`` class
*****************************
.. autoclass:: pyswig.pyswig.PySwig
    :members:
    :undoc-members:
    :show-inheritance:

The ``FileConfig`` class
*****************************
.. autoclass:: pyswig.fileconfig.FileConfig
    :members:
    :undoc-members:
    :show-inheritance:
