Introduction
============

SWIG is a great tool to generate wrappers very easily in various
scripting languages of C/C++ libraries. But if you develop a C/C++
library and its wrapper at the same time, it has a significant flow: the
necessity to create input files distinct from the source code of your
library. The more extensive the library is, the biggest issue is to have
to maintain virtually two sets of header files.

With PySwig, the source header files are annotated, allowing the
automatic generation of the input files needed by Swig. With those input
files, Swig can then create the sources of the wrapper. In this way, the
maintenance effort is reduced to its minimum, since only one set of
header files is needed.

Furthermore, PySwig can also take care of calling Swig to generate the
wrapper. The result is a Python script, importing PySwig, and defining
the operations to carry out. In the end, one just has to call this
Python script to generate the Swig input files and call Swig to create
the wrapper source files in one step.

Note that this document is automatically generated from the file
readme.xml by the tool ${msword2md\_version}. So don’t modify the
README.md file directly because all changes will be lost. Modify the
readme.xml instead.

Annotations
===========

The basic principle is to add XML-like annotations in comments to
various effects: to comment out a line, or a block of code; to include
some lines in the input files used by Swig; to add automatically include
files.

Add source include file
-----------------------

::

    //<swig_inc/>

Automatically add the include file to the Swig input file. As an example
of usage of this annotation, the result in the Swig input file would be:

::

    //<swig_inc>
    %{
    #include "simple/object.h"
    %}
    //</swig_inc>

Comment out one line
--------------------

::

    //<swig_out/>

This annotation is to comment out one line of source code in the Swig
input file. See the line below as an example one line in a header file
of a library.

::

    std::string &get_name();   //<swig_out/>   In C#, string are immutable

This same line will be transformed to the following line in the Swig
input file:

::

    //     std::string &get_name();    //<swig_out/>   In C#, string are immutable

Commenting out a block of code
------------------------------

Often a block of code needs to be commented out. It can be done in this
way.

::

    //<swig_out>
        int m_value = 0;
        …
    //</swig_out>

The result in the Swig input file is:

::

    /* //<swig_out>
        int m_value = 0;
        …
    //</swig_out> */

Add code specific to Swig
-------------------------

Often, it’s required to add code specific to Swig to handle templates,
typemaps, director, etc. It can be done as follow:

::

    /*<swig>
    %apply std::string &OUTPUT …
    //</swig>*/

It will result in the following line in a Swig input file:

::

    //<swig>
    %apply std::string &OUTPUT …
    //</swig>

To add only one line, a simpler form is also possible:

::

    //<swig/> %apply std::string &OUTPUT …

In the Swig input file, the line will look like the following:

::

    %apply std::string &OUTPUT …

Build script
============

As mentioned earlier, PySwig can also be used to automate the creation
of the wrapper by directly calling Swig. The simplest is to look at one
of the examples provided in the git repo for the wrapper of the c++
library
`simple <https://gitlab.com/libesys/tools/pyswig/-/blob/master/src/pysimple.py>`__.

There are only 2 Python classes to be aware of:

-  FileConfig, for the configuration for a set of source files

-  Swig, used to control the whole process of generating the input files
   for Swig and calling Swig


