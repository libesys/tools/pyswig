##
## __legal_b__
##
## Copyright (c) 2021 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import nox
import os
import shutil
import anybadge

def recursive_overwrite(src, dest, ignore=None):
    if os.path.isdir(src):
        if not os.path.isdir(dest):
            os.makedirs(dest)
        files = os.listdir(src)
        if ignore is not None:
            ignored = ignore(src, files)
        else:
            ignored = set()
        for f in files:
            if f not in ignored:
                recursive_overwrite(os.path.join(src, f), 
                                    os.path.join(dest, f), 
                                    ignore)
    else:
        shutil.copyfile(src, dest)

@nox.session(python=["3.5", "3.6", "3.7", "3.8", "3.9"])
def unit_tests(session):
    print("Python version : %s" % session.python)
    output_folder = "build/test-results-py" + session.python.replace(".", "")
    badge_file = "public/python"+ session.python.replace(".", "") + "-badge.svg"
    print("Output folder  : %s" % output_folder)
    session.install("unittest-xml-reporting")
    os.makedirs("public", exist_ok=True)

    # Write badge for failed unit tests
    session.log("Write badge for failed unit tests")
    thresholds = {"Failed": 'red',
                "Ok": 'green'}
    badge = anybadge.Badge('Python '+ session.python, "Failed", thresholds=thresholds)
    badge.write_badge(badge_file, overwrite=True)

    result = session.run("python", "-m", "xmlrunner",  "discover", "-s", "src", "-o", output_folder)
    if result:
        # Overwrite badge after unit tests passed
        session.log("Overwrite badge after unit tests passed")
        badge = anybadge.Badge('Python '+ session.python, "Ok", thresholds=thresholds)
        badge.write_badge(badge_file, overwrite=True)

@nox.session()
def coverage(session):
    session.install("unittest-xml-reporting")
    session.install("coverage")
    result = session.run("coverage", "run", "-m", "xmlrunner",  "discover", "-s", "src", "-o", "build/test-results")
    print("result = %s" % result)
    session.run("coverage", "xml", "-o", "build/coverage.xml", "--include=src/pyswig/*.*")
    session.run("coverage", "html",  "-d", "build/htmlcov", "--include=src/pyswig/*.*")
    os.makedirs("public/htmlcov", exist_ok=True)
    recursive_overwrite("build/htmlcov/", "public/htmlcov/")
    session.run("coverage", "report", "--include=src/pyswig/*.*")

@nox.session(reuse_venv=True)
def docs(session):
    session.install("sphinx") 
    session.install("sphinx-rtd-theme")
    session.install("recommonmark")
    session.install("sphinxcontrib-napoleon")
    os.chdir("docs")
    session.run("make", "html", external=True)
    os.makedirs("../public", exist_ok=True)
    recursive_overwrite("build/html/", "../public/")