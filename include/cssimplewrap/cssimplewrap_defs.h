/*!
 * \file cstestwrap/cstestwrap_defs.h
 * \brief Definitions needed for cstestwrap
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef CSTESTWRAP_EXPORTS
#define CSTESTWRAP_API __declspec(dllexport)
#elif CSTESTWRAP_USE
#define CSTESTWRAP_API __declspec(dllimport)
#else
#define CSTESTWRAP_API
#endif



