/*!
 * \file pysimple/pyversion.h
 * \brief Version info for pytest
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define PYSIMPLE_MAJOR_VERSION    0
#define PYSIMPLE_MINOR_VERSION    0
#define PYSIMPLE_RELEASE_NUMBER   1
#define PYSIMPLE_VERSION_STRING   "pysimple 0.0.1"

// Must be updated manually as well each time the version above changes
#define PYSIMPLE_VERSION_NUM_DOT_STRING   "0.0.1"
#define PYSIMPLE_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define PYSIMPLE_VERSION_NUMBER (PYSIMPLE_MAJOR_VERSION * 1000) + (PYSIMPLE_MINOR_VERSION * 100) + PYSIMPLE_RELEASE_NUMBER
#define PYSIMPLE_BETA_NUMBER      1
#define PYSIMPLE_VERSION_FLOAT PYSIMPLE_MAJOR_VERSION + (PYSIMPLE_MINOR_VERSION/10.0) + (PYSIMPLE_RELEASE_NUMBER/100.0) + (PYSIMPLE_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define PYSIMPLE_CHECK_VERSION(major,minor,release) \
    (PYSIMPLE_MAJOR_VERSION > (major) || \
    (PYSIMPLE_MAJOR_VERSION == (major) && PYSIMPLE_MINOR_VERSION > (minor)) || \
    (PYSIMPLE_MAJOR_VERSION == (major) && PYSIMPLE_MINOR_VERSION == (minor) && PYSIMPLE_RELEASE_NUMBER >= (release)))



