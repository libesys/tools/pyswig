/*!
 * \file pysimple/pysimple_defs.h
 * \brief Definitions needed for pysimple
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef PYSIMPLE_EXPORTS
#define PYSIMPLE_API __declspec(dllexport)
#elif PYSIMPLE_USE
#define PYSIMPLE_API __declspec(dllimport)
#else
#define PYSIMPLE_API
#endif



