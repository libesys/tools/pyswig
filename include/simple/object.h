/*!
 * \file simple/object.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "simple/simple_defs.h"

#include <string>
#include <stdint.h>

//<swig_inc/>

namespace simple
{

class SIMPLE_API Object
{
public:
    Object();
    Object(const std::string &name);
    virtual ~Object();

    void set_name(const std::string &name);
    std::string &get_name();                    //<swig_out/>   In C#, string are immutable 
    const std::string &get_name() const;    

    int set_data(uint8_t *data, uint32_t size);
    int get_data(uint8_t *data, uint32_t &size);
    uint8_t *get_data(uint32_t &size);

protected:
    std::string m_name;
    uint8_t m_data[256];
    uint32_t m_size = 0;
};

}