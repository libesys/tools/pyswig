/*!
 * \file simple/version.h
 * \brief Version info for simple
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define SIMPLE_MAJOR_VERSION    0
#define SIMPLE_MINOR_VERSION    0
#define SIMPLE_RELEASE_NUMBER   1
#define SIMPLE_VERSION_STRING   "Simple 0.0.1"

// Must be updated manually as well each time the version above changes
#define SIMPLE_VERSION_NUM_DOT_STRING   "0.0.1"
#define SIMPLE_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define SIMPLE_VERSION_NUMBER (SIMPLE_MAJOR_VERSION * 1000) + (SIMPLE_MINOR_VERSION * 100) + SIMPLE_RELEASE_NUMBER
#define SIMPLE_BETA_NUMBER      1
#define SIMPLE_VERSION_FLOAT SIMPLE_MAJOR_VERSION + (SIMPLE_MINOR_VERSION/10.0) + (SIMPLE_RELEASE_NUMBER/100.0) + (SIMPLE_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define SIMPLE_CHECK_VERSION(major,minor,release) \
    (SIMPLE_MAJOR_VERSION > (major) || \
    (SIMPLE_MAJOR_VERSION == (major) && SIMPLE_MINOR_VERSION > (minor)) || \
    (SIMPLE_MAJOR_VERSION == (major) && SIMPLE_MINOR_VERSION == (minor) && SIMPLE_RELEASE_NUMBER >= (release)))



