/*!
 * \file simple/simple_defs.h
 * \brief Definitions needed for simple
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef SIMPLE_EXPORTS
#define SIMPLE_API __declspec(dllexport)
#elif SIMPLE_USE
#define SIMPLE_API __declspec(dllimport)
#else
#define SIMPLE_API
#endif



